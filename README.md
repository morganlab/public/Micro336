**Prerequisites**

All Micro336 exercises can be carried out in the University of Otago Student Desktop environment, which has R and R Studio preinstalled.

You can also install R and R Studio on your own hardware:

1) R
    
    Download at: https://cloud.r-project.org/

2) R studio

    Download at : https://www.rstudio.com/products/rstudio/download/. 

    (Choose R Studio Desktop - Open Source License)


Lab errata:

*If Lab2 doesn't knit in R studio but all chunks work OK when run individually:
	**knit(Lab2.RMD)
	**render(Lab2.Rm)

	To produce pdf	

*This is also how Lab3 should knit. This requires the libraries "knitr" and "rmarkdown" to be loaded


