remove.packages(c("phyloseq", "vegan"))
install.packages("vegan", type="source")
source("https://bioconductor.org/biocLite.R")
biocLite("phyloseq", type="source")
