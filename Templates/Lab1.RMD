---
title: "My First Micro336 Assignment"
author: "Your Name Not My Name"
date: "Today's Date"
output:
  html_document: default
  pdf_document: default
---

This is an extremely simple R Markdown example. You can use the white space to write comments and your answers to any questions that are in the lab manual. 

R will execute commands that are between three backticks followed by {r}, like the code block underneath this sentence.

```{r, chunk_alpha}
#R will not execute this comment because it starts with #, but it will execute the code underneath
print ("Welcome to Micro336!")
```

The preceding three backticks indicate the end of the block of code, so I can put more comments here.

```{r, chunk_beta}
# This is my second chunk of R code
print("I can't wait to learn to use R!")
a<-1
b<-2
print(a + b)
```

This is where we will enter our code to get a preview of some of R's powerful features.

Put your "chunk 3" code here. Try modifying it to plot some other relationship.
```{r, chunk_gamma}

```


```{r, chunk1_setup, echo=FALSE, message=FALSE, warning=FALSE}

library(phyloseq)
library(dada2)
library(dplyr)
library(ggplot2)
```

```{r, chunk2_fileparse, echo=TRUE, message=FALSE, warnings=FALSE}

```

```{r, chunk3_dadaQC, echo=FALSE, message=FALSE}

```

```{r, chunk4_runDADA, echo=FALSE, message=FALSE}

```

```{r, chunk5_chimeras, echo=FALSE, message=FALSE}

```


```{r, chunk6_tally, echo=FALSE, message=FALSE}

```

```{r, chunk7_taxonomy, echo=FALSE, message=FALSE}

```

```{r, chunk8_physeq, echo=FALSE, message=FALSE}

```

```{r, chunk9_evaluate, echo=FALSE, message=FALSE}

```

```{r, chunk10_richness, echo=FALSE, message=FALSE}

```

```{r, chunk11_barplots, echo=FALSE, message=FALSE}

```

```{r, chunk12_intersections, echo=FALSE, message=FALSE}

```

```{r, chunk13_finale, echo=FALSE, message=FALSE}

```

```{r, chunk14_rarefy}

```
