---
title: "import"
output: pdf_document
---

```{r setup, include=FALSE}
library(phyloseq)
library(biomformat)
setwd("~/Dropbox/Otago/Teaching/Micro336/Lab info/Assessment/")
biom_otu_tax <- import_biom("Dataset-RISK-stool/440_otu_table.biom")
meta <- import_qiime_sample_data("Dataset-RISK-stool/44983_mapping_file.txt")
merged <- merge_phyloseq(biom_otu_tax, merged)
merged

# hint - an example of how to take a subset
# my_subset<- subset_samples(HMPv35, (HMPbodysubsite=="Tongue_dorsum" | HMPbodysubsite=="Palatine_Tonsils"))
```

